Midlertidige krav til projekt

- skal kunne måle flere steder
- skal kunne sende advarsel som SMS (https://randomnerdtutorials.com/esp32-sim800l-send-text-messages-sms/)
- skal kunne måle temperature både under og over ideele temperaturer
- skal have backup batteri, der slår til i tilfælde af strømafbrydelse
