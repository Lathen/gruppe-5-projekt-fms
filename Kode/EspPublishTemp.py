from umqtt.simple import MQTTClient
import ubinascii
from machine import Pin
from time import sleep
import dht 
import mip

sensor = dht.DHT22(Pin(15))

mqtt_server = '192.168.87.62'  # Example MQTT broker
client_id = ubinascii.hexlify(machine.unique_id())
topic_pub = b'esp32/dht/TempSensor1'

def do_connect(ssid, password):
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())

def sub_cb(topic, msg):
    print((topic, msg))

def connect():
    global client_id, mqtt_server
    client = MQTTClient(client_id, mqtt_server, port=1883)
    client.set_callback(sub_cb)
    client.connect()
    print('Connected to %s MQTT broker \n' %mqtt_server)
    return client

def publish_msg(client, topic, msg):
    client.publish(topic, msg)
    print('Published %s to %s' % (msg, topic))

#do_connect('SnowdenAppreciationHotspot', 'leaks1337') # Only 2.4 Ghz
do_connect('Phone_1_2058', 'dhpk9h5n4w6gups') # Only 2.4 Ghz

mip.install("umqtt.simple")
print("")

try:
    client = connect()
except OSError as e:
    print(e)

while True:
  try:
    sleep(4)
    sensor.measure()
    temp = sensor.temperature()
    hum = sensor.humidity() # to be used later
    publish_msg(client, topic_pub, '%s' % temp)
    print('Temperature: %3.1f C' %temp)
  except OSError as e:
    print('Failed to read sensor.')