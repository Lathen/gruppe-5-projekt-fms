Project Management 

- Temperatur måler til datacenter
- Use case: 
        - Bruger vil kunne overvåge temperaturen i deres datacenter.  
        - Sensorer måler temperaturer konstant og hvis den overstiger en på forhånd defineret værdi, så sendes der en alarm til en ansvarlig person.
        - Modulet kan udvides med flere sensorer i takt med at det bliver nødvendigt. 

Problemformulering:
    - Datacenter er følsom overfor høje temperaturer. Ansvarlig medarbejder vil gerne kunne modtage en besked/advaresel hvis temperaturen overstiger en på forhånd defineret grænseværdi. De har så henvendt sig til os for at få vores forslag til en løsning, der både er nem at implementere, vedligeholde og igangsætte. 
    - 